import numpy as np

'''The aim of this script is to be able to derive the bunch length from tomoscope measurements as well as apply the routine to self-generated data'''

def time_axis(NSamples: float,HScale: float):
    return np.linspace(0,(NSamples-1)*HScale,NSamples)

# prova

xaxis = time_axis(1000,0.5)
print(xaxis)

def prof_reshape(profiles: np.array, NTraces: int, NBins: int):
    return np.reshape(profiles,(NTraces,NBins))

def FWHM_profiles(profile: np.array, HScale: float, mode: str = 'ave'):
    if (mode != 'ave' and mode !='filt'):
        raise(Exception("Mode not available. Select between 'ave' and 'filt'."))

    NSamples = len(profile)
    time_axis = np.linspace(0,(NSamples-1)*HScale,NSamples)

def BL_ave(profile: np.array, H_Scale: float):
    """
    Computing bunch length. Averaging points to reduce the noise.
    """
    N_Samples = len(profile)
    x_axis = np.linspace(0, (N_Samples-1) * H_Scale, (N_Samples))

    # Finding the "zero" line
    min_idx = np.where(profile == np.min(profile))[0]
    oneside = int(len(profile)/20)
    neighbourhood = np.linspace(min_idx-oneside, min_idx+oneside, 2*oneside+1)
    neighbourhood = neighbourhood.astype(int)
    if np.any(neighbourhood < 0):
        log.info("Neighbourhood < 0. Cutting it.")
        firstbad = np.where(neighbourhood <= 0)[0][-1]
        neighbourhood = neighbourhood[firstbad:]
    if np.any(neighbourhood >= N_Samples):
        log.info("Neighbourhood > N Samples. Cutting it.")
        firstbad = np.where(neighbourhood >= N_Samples)[0][0]
        neighbourhood = neighbourhood[:firstbad]
    bottom = np.average(profile[neighbourhood])

    # Find the max value
    max_idx = np.where(profile == np.max(profile))[0][0]
    max_value = np.average(profile[max_idx-1:max_idx+2])

    # Find height and halfline
    max_point = (max_idx*H_Scale, max_value)
    height = max_point[1] - bottom
    half = height/2
    halfline = half + bottom

    # Finding the closest points to the FWHM line
    lower_right_idx = max_idx
    while(profile[lower_right_idx] > halfline):
        lower_right_idx += 1
    lower_left_idx = max_idx
    while(profile[lower_left_idx] > halfline):
        lower_left_idx -= 1
    higher_left_idx = lower_left_idx + 1
    while(profile[higher_left_idx] < profile[lower_left_idx]):
        higher_left_idx += 1
    higher_right_idx = lower_right_idx - 1
    while(profile[higher_right_idx] < profile[lower_right_idx]):
        higher_right_idx -= 1

    # Fit & find the x value of the left point
    coeff_left = np.polyfit(x_axis[lower_left_idx:higher_left_idx+1],
                            profile[lower_left_idx:higher_left_idx+1], 1)
    left_point = ((halfline-coeff_left[1])/coeff_left[0], halfline)

    # Fit & find the x value of the right point
    coeff_right = np.polyfit(x_axis[higher_right_idx:lower_right_idx+1],
                             profile[higher_right_idx:lower_right_idx+1], 1)
    right_point = ((halfline-coeff_right[1])/coeff_right[0], halfline)

    return right_point[0]-left_point[0], left_point, right_point


def BL_filt(profile: np.array, H_Scale: float):
    """
    Computing bunch length. Filtering to reduce the noise.
    """
    N_Samples = len(profile)
    x_axis = np.linspace(0, (N_Samples-1) * H_Scale, (N_Samples))

    # Filtering
    order = 6
    fs = 0.2  # Sample rate, Hz
    cutoff = 0.015  # Desired cutoff frequency of the filter, Hz
    b, a = butter(order, cutoff, fs=fs, btype='low', analog=False)
    profile = filtfilt(b, a, profile)

    # Finding the "zero" line
    min_idx = np.where(profile == np.min(profile))[0][0]
    bottom = profile[min_idx]

    # Find the max value
    max_idx = np.where(profile == np.max(profile))[0][0]
    max_value = profile[max_idx]

    # Find height and halfline
    max_point = (max_idx*H_Scale, max_value)
    height = max_point[1] - bottom
    half = height/2
    halfline = half + bottom

    # Finding the closest points to the FWHM line
    lower_right_idx = max_idx
    while(profile[lower_right_idx] > halfline):
        lower_right_idx += 1
    lower_left_idx = max_idx
    while(profile[lower_left_idx] > halfline):
        lower_left_idx -= 1
    higher_left_idx = lower_left_idx + 1
    while(profile[higher_left_idx] < profile[lower_left_idx]):
        higher_left_idx += 1
    higher_right_idx = lower_right_idx - 1
    while(profile[higher_right_idx] < profile[lower_right_idx]):
        higher_right_idx -= 1

    # Fit & find the x value of the left point
    coeff_left = np.polyfit(x_axis[lower_left_idx:higher_left_idx+1],
                            profile[lower_left_idx:higher_left_idx+1], 1)
    left_point = ((halfline-coeff_left[1])/coeff_left[0], halfline)

    # Fit & find the x value of the right point
    coeff_right = np.polyfit(x_axis[higher_right_idx:lower_right_idx+1],
                             profile[higher_right_idx:lower_right_idx+1], 1)
    right_point = ((halfline-coeff_right[1])/coeff_right[0], halfline)

    return right_point[0]-left_point[0], left_point, right_point


def BL_common(data_array, H_Scale):
    """ from blond_common.fitting.profile module, with default FitOptions
    """
    N_Samples = len(data_array)
    time_array = np.linspace(0, (N_Samples-1) * H_Scale, (N_Samples))

    # Removing baseline
    profileToFit = data_array-np.mean(data_array[0:3])

    # Time resolution
    time_interval = time_array[1] - time_array[0]

    # Max and xFator times max values (defaults to half max)
    maximum_value = np.max(profileToFit)
    half_max = 0.5 * maximum_value

    # First aproximation for the half maximum values
    taux = np.where(profileToFit >= half_max)
    taux1 = taux[0][0]
    taux2 = taux[0][-1]

    # Interpolation of the time where the line density is half the maximum
    if taux1 == 0:
        t1 = time_array[taux1]
        log.warninig('FWHM is at left boundary of profile!')
    else:
        t1 = time_array[taux1] - (profileToFit[taux1]-half_max) \
            / (profileToFit[taux1] - profileToFit[taux1-1]) * time_interval

    if taux2 == (len(profileToFit)-1):
        t2 = time_array[taux2]
        log.warning('FWHM is at right boundary of profile!')
    else:
        t2 = time_array[taux2] + (profileToFit[taux2]-half_max) \
            / (profileToFit[taux2] - profileToFit[taux2+1]) * time_interval

    # Adjusting the FWHM with some scaling factor
    # bunchLengthFactor = 1

    # fwhm = bunchLengthFactor * (t2-t1)
    # center = (t1+t2)/2

    left = (t1, half_max+np.mean(data_array[0:3]))
    right = (t2, half_max+np.mean(data_array[0:3]))

    return right[0]-left[0], left, right


