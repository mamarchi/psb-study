import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pydaq
import pylan.classes.data_manager as dm
import pylan.classes.pipeline as pp
import scipy.optimize as opt
import glob
import os
import typing as typ
import warnings


# print(paths_list)
# print(type(paths_list[0]))
skip = 2
datamanager = dm.DataManager()

path = '/home/mmarchi/Documents/cernbox/SingleHarmonicInstability/Tomo_Measurements'
name = 'Data'

datamanager.load_from_pydaq('npy',path,name,alias=['BCT','Tomoscope_FlatTop','VRFH1','VRFH1S5','VRFH2','VRFH2S5','VRFHBU','VRFHBUS5','Tomoscope_Instability','VRFH1S13','VRFH1S7','VRFH2S13','VRFH2S7','VRFHBUS13','VRFHBUS7'])

# useful functions
def time_axis(NSamples: float,HScale: float):
    return np.arange(0,(NSamples)*HScale,HScale)

def reshape_profiles(profiles: np.array, N_Traces: int, N_Samples: int):
    return np.reshape(profiles, (N_Traces, N_Samples))


def FWHM_edges(profile: np.array, H_Scale: float, mode: str = "ave"):

    if (mode != "ave" and mode != "filt"):
        raise(Exception("`mode` not recognised. Please choose between `ave` and `filt`."))

    N_Samples = len(profile)
    x_axis = np.linspace(0, (N_Samples-1) * H_Scale, (N_Samples))

    if (mode == "ave"):
        # Finding the "zero" line
        min_idx = np.where(profile == np.min(profile))[0]
        oneside = int(len(profile)/20)
        neighbourhood = np.linspace(min_idx-oneside, min_idx+oneside, 2*oneside+1)
        neighbourhood = neighbourhood.astype(int)
        if (np.any(neighbourhood < 0)):
            warnings.warn("Neighbourhood < 0. Cutting it.")
            firstbad = np.where(neighbourhood <= 0)[0][-1]
            neighbourhood = neighbourhood[firstbad:]
        if (np.any(neighbourhood >= N_Samples)):
            warnings.warn("Neighbourhood > N Samples. Cutting it.")
            firstbad = np.where(neighbourhood >= N_Samples)[0][0]
            neighbourhood = neighbourhood[:firstbad]
        bottom = np.average(profile[neighbourhood])

        # Find the max value
        max_idx = np.where(profile == np.max(profile))[0][0]
        max_value = np.average(profile[max_idx-1:max_idx+2])

    elif(mode == "filt"):
        # Filtering
        order = 6
        fs = 0.2  # Sample rate, Hz
        cutoff = 0.015  # Desired cutoff frequency of the filter, Hz
        b, a = butter(order, cutoff, fs=fs, btype='low', analog=False)
        profile = filtfilt(b, a, profile)

        # Finding the "zero" line
        min_idx = np.where(profile == np.min(profile))[0][0]
        bottom = profile[min_idx]

        # Find the max value
        max_idx = np.where(profile == np.max(profile))[0][0]
        max_value = profile[max_idx]

    # Find height and halfline
    max_point = (max_idx*H_Scale, max_value)
    height = max_point[1] - bottom
    half = height/2
    halfline = half + bottom

    # Finding the closest points to the FWHM line
    lower_right_idx = max_idx
    while(profile[lower_right_idx] > halfline):
        lower_right_idx += 1
    lower_left_idx = max_idx
    while(profile[lower_left_idx] > halfline):
        lower_left_idx -= 1
    higher_left_idx = lower_left_idx + 1
    while(profile[higher_left_idx] < profile[lower_left_idx]):
        higher_left_idx += 1
    higher_right_idx = lower_right_idx - 1
    while(profile[higher_right_idx] < profile[lower_right_idx]):
        higher_right_idx -= 1

    # Fit & find the x value of the left point
    coeff_left = np.polyfit(x_axis[lower_left_idx:higher_left_idx+1],
                            profile[lower_left_idx:higher_left_idx+1], 1)
    left_point = ((halfline-coeff_left[1])/coeff_left[0],  halfline)

    # Fit & find the x value of the right point
    coeff_right = np.polyfit(x_axis[higher_right_idx:lower_right_idx+1],
                             profile[higher_right_idx:lower_right_idx+1], 1)
    right_point = ((halfline-coeff_right[1])/coeff_right[0], halfline)

    return left_point, right_point



def FWHM(profile: np.array, H_Scale: float, mode: str = "ave"):

    left_point, right_point = FWHM_edges(profile, H_Scale, mode)

    # Calculating FWHM
    FWHM = right_point[0] - left_point[0]

    return FWHM

def BunchCentre(profile: np.array, H_Scale: float, mode: str = "ave"):

    left_point, right_point = FWHM_edges(profile, H_Scale, mode)

    bunch_centre = (left_point[0] + right_point[0]) / 2.

    return bunch_centre




"Flat-Top"
shot_23 = np.array([1,2,3,4,5,6,7,8])


shot23_list = []
# waterfall_flat_top = datamanager.data['Tomoscope_FlatTop'][shot]

def give_wf():
    for s in shot_23:
        path_wf_23 = f'/home/mmarchi/Documents/cernbox/SingleHarmonicInstability/Tomo_Measurements/Data/Tomoscope_FlatTop/Shot#{s}.npy'
        wf_data_23 = np.load(path_wf_23, allow_pickle=True)
        wf_horizontal_shape = wf_data_23.shape[1]
        wf_vertical_shape = wf_data_23.shape[0]
        pk_density = np.max(wf_data_23[skip:], axis=1)
        # plt.plot(pk_density)
        # plt.contourf(wf_data_23, levels=30)
        # plt.show()
        # bin_center = int(x_shape / 2)
        # # plt.plot(wf_data_23[:, bin_center])
        # # plt.show()
        # # maxVals = np.max(wf_data_23, axis=0)
        # # plt.plot(maxVals)
        # # plt.show()
        xaxis_rawdata = np.arange(0, wf_horizontal_shape + 1, 1)
        yaxis_rawdata = np.arange(0, wf_vertical_shape + 1, 1)
        xaxis = time_axis(100, 0.5)
        return wf_data_23

waterfalls = give_wf()

profiles = reshape_profiles(waterfalls,400,1000)

print(profiles.shape[0])
print(profiles.shape[1])

hscale = 1

### Prova con una sola waterfall

path= '/home/mmarchi/Documents/cernbox/SingleHarmonicInstability/Tomo_Measurements/Data/Tomoscope_FlatTop/Shot#3.npy'
wf = np.load(path, allow_pickle=True)

# plt.contourf(wf,levels=30)
# plt.show()
plt.plot(wf[skip,:])
plt.show()
bunch_profile = wf[skip,:]
bunch_profile_avg = np.mean(bunch_profile)
avg_line = np.arange(0,999)






# fwhm = FWHM(bunch_prof,hscale,"ave")
# print(fwhm)
# plt.plot(wf[1,:],fwhm,'o')
# plt.show()



# for wf in profiles:
#     wf_horizontal_shape = wf.shape[1]
#     wf_vertical_shape = wf.shape[0]
#     xaxis_rawdata = np.arange(0, wf_horizontal_shape + 1, 1)
#     yaxis_rawdata = np.arange(0, wf_vertical_shape + 1, 1)
#     edges_fwhm = FWHM_edges(wf,hscale,'ave')
#     print(edges_fwhm)
#     fwhm = FWHM(wf,hscale,'ave')
#     print(fwhm)
#     bunch_center = BunchCentre(wf,hscale,'ave')
#     print(bunch_center)
#     for x in xaxis_rawdata:
#         prof = wf[x,:]
#         plt.plot(prof)
#         plt.plot(prof,edges_fwhm,'o')
#         plt.show()



















    # for x in xaxis_rawdata:
    #     profile = wf_data_23[x,:]
    #     # print(profile)
    #     # plt.plot(profile)
    #     # plt.show()
    #     print(len(profile))
    #     profile_list.append(profile)
    #     # print(profile_list)
    #     profiles = np.array(profile_list)
    #     print(profiles)
    #     edges = FWHM_edges(profiles,1,"ave")
#








# plt.show()


# def bunch_profiles(NSamples: float, HScale: float,NTraces: float, profiles: np.array):
#     wf_rawdata = np.load(path_rawdata, allow_pickle=True)




# shot_20 = np.array([9,10,11,12,13,14])
#
# for s in shot_20:
#     path_wf_20 = f'/home/mmarchi/Documents/cernbox/SingleHarmonicInstability/Tomo_Measurements/Data/Tomoscope_FlatTop/Shot#{s}.npy'
#     wf_data_20 = np.load(path_wf_20, allow_pickle=True)
#     x_array = wf_data_20.shape[1]
#     y_array = wf_data_20.shape[0]
#     # print(x_array)
#     # print(y_array)
#     plt.contourf(wf_data_20, levels=30)
#     plt.show()
#     plt.figure()
#     bin_center = int(x_array / 2)
#     plt.plot(wf_data_20[:, bin_center])
#     plt.show()
#     maxVals = np.max(wf_data_20, axis=1)
#     plt.plot(maxVals)
#     plt.show()




















